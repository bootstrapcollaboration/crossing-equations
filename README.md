## Work in progress

This is a framework for reading conformal blocks and packaging them into crossing equations that can easily be given to SDPB. Although similar to [PyCFTBoot](https://github.com/cbehan/pycftboot) and [CBoot](https://github.com/tohtsky/cboot), it is being written with three core principles:
  1. A smaller memory footprint.
  2. A table format that allows spinning operators.
  3. Taking advantage of all tasks that are embarrassingly parallel.

Contributors are welcome!
