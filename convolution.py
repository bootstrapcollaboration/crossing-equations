#!/usr/bin/env python2
"""
This assumes conformal blocks have already been given in a standard format
(possibly XML) and supports convolutions and linear combinations.
"""
from __future__ import print_function
from symengine.lib.symengine_wrapper import *
import multiprocessing

# Use x for "delta" and y for "delta_ext"
x = Symbol('x')
y = Symbol('y')
# Override this to use a different number of cores
max_threads = multiprocessing.cpu_count()

prec = 660
small = 1e-10
r_cross = eval_mpfr(3 - 2 * sqrt(2), prec)

# No need for the user to call this
def convolve_single(infile, rho, delta_ext, symmetric, pv):
    # Do stuff
    return

"""
Class for labelling blocks since there is a lot in the spinning case. Tensor
structures are labelled with a simple integer. However, block tables will define
this at the beginning. The definition will be a list of coefficients in terms of
a standard basis that will be published somewhere.
"""
class BlockLabel:
    def __init__(self, irrep, three_point_left, three_point_right, four_point):
        self.irrep = irrep
        self.three_point_left = three_point_left
        self.three_point_right = three_point_right
        self.four_point = four_point

"""
Class for PolynomialVectors where the first argument is usually a list.
It could also be a filepath for memory-intensive problems.
"""
class PolynomialVector:
    def __init__(self, derivatives, poles, block_label):
        self.vector = derivatives
        self.poles = poles
        self.label = block_label

    def __add___(self, new_pv):
        if type(self.vector) == type("") or type(new_pv.vector) == type(""):
            raise Exception("Cannot add vectors with references to an external file.")

        # Figure out which poles are not shared
        lpoles, rpoles = non_common_elements(self.poles, new_pv.poles)
        new_poles = self.poles + rpoles
        lprod = prod([(x - p) for p in lpoles])
        rprod = prod([(x - p) for p in rpoles])

        derivatives = []
        for i in range(0, len(self.vector)):
            derivatives.append(self.vector[i] * rprod + new_pv.vector[i] * lprod)
        # The label on the left is used so this addition is not commutative
        return PolynomialVector(derivatives, new_poles, self.block_label)

    def __rmul__(self, const):
        if type(self.vector) == type(""):
            raise Exception("Cannot rescale vectors with references to an external file.")

        derivatives = []
        for i in range(0, len(self.vector)):
            derivatives.append(const * self.vector[i])
        return PolynomialVector(derivatives, self.poles, self.block_label)

    def __rsub__(self, new_pv):
        return (-1) * self + new_pv

    def subs(self, var, val):
        if type(self.vector) == type(""):
            raise Exception("Cannot substitute value into a file.")
        const = 1.0

        if var == x:
            if "subs" in dir(val):
                # The user is shifting x, presumably by a constant
                # If she did x --> x + x^5, we could never get the poles
                shift = val.subs(x, 0)
                self.poles = [(p - shift) for p in self.poles]
                # There is an implicit (4r)^x everywhere
                const = (4 * r_cross) ** shift
            else:
                # The denominator becomes a constant if we plug in a dimension
                const /= prod([(val - p) for p in poles])
                self.poles = []

        for i in range(0, len(self.vector)):
            self.vector[i] = const * self.vector[i].subs(var, val)

"""
Class which reads a file and immediately produces convolved blocks.
Is it ever useful to have conformal blocks in memory that are not convolved?
Turning them into super blocks and such can be done after convolution.
"""
class ConvolvedBlockTable:
    def __init__(self, infile, labels, delta_ext = y, symmetric = False):
        self.dim = read_first_line(infile)
        self.delta_ext = delta_ext
        # Create empty table
        self.table = []
        for rho in labels:
            self.table.append(PolynomialVector([], [], rho))
        i = 0
        # Start filling the entries with different threads
        while i < len(labels):
            processes = []
            for j in range(i, min(i + max_threads, len(irreps))):
                p = Multiprocessing.Process(target = convolve_single, args = (infile, labels[j], delta_ext, symmetric, self.table[j]))
                p.start()
            # Telling them all to wait
            for p in processes:
                p.join()
            i += max_threads

    def dump(outfile):
        if self.delta_ext == y:
            raise Exception("Convolved blocks written to a file should not have a symbolic external dimension.")
