#!/usr/bin/env python2
"""
This should copy SDPB.m so people can use this with no learning curve.
All optimizations beyond that should happen at a higher level.
PositiveMatrixWithPrefactor should have raw vectors and start with a pole list.
It will be packaged in PolynomialVectorMatrix which includes metadata.
The argument to this high-level class should be specified in a compact way.
"""
class PositiveMatrixWithPrefactor:
    def __init__(self, poles, matrix):
        self.poles = poles
        self.matrix = matrix

class PolynomialVectorMatrix:
    def __init__(self, matrices, block_label):
        size = len(matrices[0])
        # Set the metadata
        self.label = block_label
        # For file arguments, we must remember which block is being fetched
        self.reflist = []
        for r in range(0, size):
            for s in range(0, size):
                for m in matrices:
                    if type(m[r][s].vector) == type(""):
                        self.reflist.append(m[r][s].label)

        # Create the raw object to populate
        matrix = empty_matrix(size, size, [])

        # Make sure everything has a common prefactor
        all_poles = []
        for m in matrices:
            for r in range(0, size):
                for s in range(0, size):
                    if m[r][s] == 0 or type(m[r][s].vector) == type(""):
                        continue
                    dummy, new_poles = non_common_elements(all_poles, m[r][s].poles)
                    all_poles += new_poles
        for m in matrices:
            for r in range(0, size):
                for s in range(0, size):
                    missing_poles, dummy = non_common_elements(all_poles, m[r][s].poles)
                    missing_prod = prod([(x - p) for p in missing_poles])
                    if type(m[r][s].vector) == type(""):
                        matrix[r][s].append(m[r][s].vector)
                        continue
                    for poly in m[r][s].vector:
                        matrix[r][s].append(poly * missing_prod)
        self.data = PositiveMatrixWithPrefactor(all_poles, matrix)

    def __add__(self, new_pvm):
        if len(self.reflist) > 0 or len(new_pvm.reflist) > 0:
            raise Exception("Cannot add matrices with references to an external file.")

        # Like the PolynomialVector one
        lpoles, rpoles = non_common_elements(self.data.poles, new_pvm.data.poles)
        new_poles = self.data.poles + rpoles
        lprod = prod([(x - p) for p in lpoles])
        rprod = prod([(x - p) for p in rpoles])

        size = len(self.data.matrix)
        matrix = empty_matrix(size, size, [])
        for r in range(0, size):
            for s in range(0, size):
                for i in range(0, len(self.data.matrix[r][s])):
                    matrix[r][s].append(self.data.matrix[r][s][i] * rprod + new_pvm.data.matrix[r][s][i] * lprod)
        return PolynomialVectorMatrix([matrix], self.block_label)

    def __rmul__(self, const):
        if len(self.reflist) > 0:
            raise Exception("Cannot rescale matrices with references to an external file.")

        # Like the PolynomialVector one
        size = len(self.data.matrix)
        matrix = empty_matrix(size, size, [])
        for r in range(0, size):
            for s in range(0, size):
                for el in self.data.matrix[r][s]:
                    matrix[r][s].append(const * el)
        return PolynomialVectorMatrix([matrix], self.block_label)

    def __rsub__(self, new_pvm):
        return (-1) * self + new_pvm

    def subs(self, var, val):
        if len(self.reflist) > 0:
            raise Exception("Cannot substitute value into a file.")
        const = 1.0

        # Like the PolynomialVector one
        if var == x:
            if "subs" in dir(val):
                shift = val.subs(x, 0)
                self.data.poles = [(p - shift) for p in self.data.poles]
                const = (4 * r_cross) ** shift
            else:
                const /= prod([(val - p) for p in self.data.poles])
                self.data.poles = []

        size = len(self.data.matrix)
        for r in range(0, size):
            for s in range(0, size):
                for i in range(0, len(self.data.matrix[r][s])):
                    self.data.matrix[r][s][i] = const * self.data.matrix[r][s][i].subs(var, val)

    def sandwich(self, vec):
        if len(self.reflist) > 0:
            raise Exception("Cannot replace matrix elements in a file.")

        vector = []
        size = len(self.data.matrix)
        for i in range(self.data.matrix[0][0]):
            inner_product = 0
            for r in range(0, size):
                for s in range(0, size):
                    inner_product += vec[r] * vec[s] * self.data.matrix[r][s][i]
            vector.append(inner_product)
        self.data = PositiveMatrixWithPrefactor(self.data.poles, [[vector]])

def write_bootstrap_sdp(filename, obj, norm, pms_with_prefactor):
    # Do stuff
