#!/usr/bin/env python2
"""
At some point, we will include this in all other files.
"""
def prod(array):
    result = 1.0
    for el in array:
        result *= el
    return result

def non_common_elements(list1, list2):
    i = 0
    while i < len(list1):
        el = list1[i]
        found = False
        for j in range(0, len(list2)):
            if abs(list2[j] - el) < small:
                found = True
                list1 = list1[:i] + list1[i + 1:]
                list2 = list2[:j] + list2[j + 1:]
                break
        if found == False:
            i += 1
    return (list1, list2)

def empty_matrix(m, n, entry):
    matrix = []
    for r in range(0, m):
        matrix.append([])
        for s in range(0, n):
            matrix[-1].append(entry)
    return matrix
